﻿namespace Elika.Hosting.Consul
{
    /// <summary>
    ///     Meta keys
    /// </summary>
    public class Meta
    {
        /// <summary>
        ///     HealthCheck endpoint key
        /// </summary>
        public const string HealthCheckEndPoint = "healthcheck_endpoint";

        /// <summary>
        ///     InDockerContainer key
        /// </summary>
        public const string InDockerContainer = "in_docker_container";

        /// <summary>
        ///     TCP HealthCheck Interval key
        /// </summary>
        public const string TcpHealthCheckInterval = "tcp_healthcheck_interval";

        /// <summary>
        ///     Deregister Critical Service After key
        /// </summary>
        public const string DeregisterCriticalServiceAfter = "deregister_critical_service_after";

        /// <summary>
        ///     Machine Name key
        /// </summary>
        public const string MachineName = "machine_name";

        /// <summary>
        ///     OS Version key
        /// </summary>
        public const string OSVersion = "os_version";

        /// <summary>
        ///     Runtime Version key
        /// </summary>
        public const string RuntimeVersion = "runtime_version";

        /// <summary>
        ///     Current App Version Key
        /// </summary>
        public const string AppVersion = "app_version";
    }
}
