﻿using Consul;
using Elika.Hosting.Consul.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Elika.Hosting.Consul;

internal class ConsulConfigurationSource : IConfigurationSource
{
    private readonly IHostEnvironment _environment;
    private readonly ConsulClientOptions _options;

    public ConsulConfigurationSource(IHostEnvironment environment, ConsulClientOptions options)
    {
        _environment = environment;
        _options = options;
    }

    public IConfigurationProvider Build(IConfigurationBuilder builder)
    {
        var root = Utils.GetRoot(_options, _environment);

        return new ConsulConfigurationProvider(new ConsulClient(_options), root);
    }
}