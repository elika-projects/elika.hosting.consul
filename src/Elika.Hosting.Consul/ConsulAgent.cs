﻿using Consul;
using Elika.Hosting.Consul.Options;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Elika.Hosting.Consul;

internal class ConsulAgent
{
    private readonly IHostApplicationLifetime _applicationLifetime;
    private readonly IConsulClient _client;
    private readonly ConsulAgentOptions _consulAgentOptions;
    private readonly HealthCheckTcpServer _healthCheck;
    private readonly ConsulHealthCheckOptions _healthCheckOptions;
    private readonly ILogger<ConsulAgent> _logger;
    private AgentServiceRegistration? _agentServiceRegistration;

    private AgentServiceCheck? _check;

    public ConsulAgent(
        ILogger<ConsulAgent> logger,
        IConsulClient client,
        IOptions<ConsulHealthCheckOptions> healthCheckOptions,
        IOptions<ConsulAgentOptions> consulAppOptions,
        IHostApplicationLifetime applicationLifetime,
        HealthCheckTcpServer healthCheck
    )
    {
        _logger = logger;
        _client = client;
        _consulAgentOptions = consulAppOptions.Value;
        _applicationLifetime = applicationLifetime;
        _healthCheck = healthCheck;
        _healthCheckOptions = healthCheckOptions.Value;
    }

    private AgentServiceCheck GetCheck()
    {
        if (_check is not null)
            return _check;

        _check = new AgentServiceCheck
        {
            Interval = _healthCheckOptions.Interval,
            DeregisterCriticalServiceAfter = _healthCheckOptions.DeregisterCriticalServiceAfter,
            TCP = $"{_healthCheckOptions.Address}:{_healthCheckOptions.Port}",
            Name = _healthCheckOptions.CheckName,
            Notes = _healthCheckOptions.Notes
        };

        if (_consulAgentOptions.IsInDockerContainer)
            _check.DockerContainerID = Environment.MachineName;

        return _check;
    }

    private AgentServiceRegistration GetRegistration()
    {
        if (_agentServiceRegistration is not null)
            return _agentServiceRegistration;


        var check = GetCheck();

        _agentServiceRegistration = new AgentServiceRegistration
        {
            ID = _consulAgentOptions.AppId,
            Name = _consulAgentOptions.AppName,
            Check = check,
            Meta = _consulAgentOptions.Meta.ToDictionary(k => k.Key, v => v.Value.ToString()),
            Address = _consulAgentOptions.Address.ToString(),
            Port = _consulAgentOptions.Port,
            Tags = _consulAgentOptions.Tags.ToArray()
        };

        return _agentServiceRegistration;
    }

    private async Task RegisterAsync(CancellationToken cancellationToken = default)
    {
        _logger.LogInformation("Registering in Consul");
        var registration = GetRegistration();
        await _client.Agent.ServiceRegister(registration, cancellationToken);
        _logger.LogInformation("Registered in Consul");
    }

    private async Task DeregisterAsync(CancellationToken cancellationToken = default)
    {
        if(_agentServiceRegistration is null)
            return;

        _logger.LogInformation("Deregistering in Consul");
        await _client.Agent.ServiceDeregister(_agentServiceRegistration.ID, cancellationToken);
        _logger.LogInformation("Deregistered in Consul");
    }

    private void Deregister()
    {
        DeregisterAsync().Wait();
    }

    public async Task RunAsync(CancellationToken cancellationToken = default)
    {
        await RegisterAsync(cancellationToken);
        _applicationLifetime.ApplicationStopping.Register(Deregister);
        _healthCheck.Run(_healthCheckOptions.Address, _healthCheckOptions.Port);
    }
}