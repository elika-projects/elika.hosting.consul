﻿using System.Net;
using System.Reflection;
using Consul;
using Elika.Hosting.Consul.BackgroundServices;
using Elika.Hosting.Consul.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace Elika.Hosting.Consul;

/// <summary>
///     Extensions for <see cref="IHostBuilder"/>
/// </summary>
public static class HostBuilderExtensions
{
    /// <summary>
    ///     Add Consul
    /// </summary>
    /// <param name="builder">Builder</param>
    /// <remarks>
    ///     If you need to change Consul connection, then use <see cref="ConsulClientOptions" />
    ///     <code></code>
    ///     If you need to change service information in Consul, then use <see cref="ConsulAgentOptions" />
    ///     <code></code>
    ///     If you need to change health check in Consul, then use <see cref="ConsulHealthCheckOptions" />
    ///     <code></code>
    ///     Example usage:
    ///     <code>
    ///     services.Configure&lt;ConsulHealthCheckOptions&gt;(options => { ... });
    ///     </code>
    ///     or if you need services from <see cref="IServiceProvider" />, then use:
    ///     <code>
    ///     services.AddOptions&lt;ConsulHealthCheckOptions&gt;()
    ///             .Configure&lt;ExampleService&gt;((options, service) => { .. });
    ///     </code>
    /// </remarks>
    /// <returns>Builder</returns>
    public static IHostBuilder UseConsul(
        this IHostBuilder builder
    )
    {
        ArgumentNullException.ThrowIfNull(builder);

        return builder
            .ConfigureAppConfiguration((host, configurations) =>
            {
                var config = configurations.Build();
                var consulClientOptions = config.GetSection(ConsulClientOptions.Key).Get<ConsulClientOptions>() ?? new ConsulClientOptions();
                configurations.Add(new ConsulConfigurationSource(host.HostingEnvironment, consulClientOptions));
            })
            .ConfigureServices((host, services) =>
            {
                services.AddOptions<ConsulClientOptions>()
                    .Bind(host.Configuration.GetSection(ConsulClientOptions.Key))
                    .ValidateDataAnnotations()
                    .PostConfigure<IOptions<ConsulAgentOptions>>((options, agent) =>
                    {
                        if (string.IsNullOrEmpty(options.KVApp) || string.IsNullOrWhiteSpace(options.KVApp))
                            options.KVApp = agent.Value.AppName;
                    });

                services.AddOptions<ConsulAgentOptions>()
                    .Bind(host.Configuration.GetSection(ConsulAgentOptions.Key))
                    .ValidateDataAnnotations()
                    .PostConfigure<IOptions<ConsulHealthCheckOptions>, IHostEnvironment>((options, health, env) =>
                    {
                        options.AppId ??= env.ApplicationName;
                        options.AppName ??= env.ApplicationName;

                        if (options.Port is 0)
                            options.Port = health.Value.Port;

                        var val = health.Value;

                        options.Meta.TryAdd(Meta.HealthCheckEndPoint, new IPEndPoint(val.Address, val.Port));
                        options.Meta.TryAdd(Meta.InDockerContainer, options.IsInDockerContainer);
                        options.Meta.TryAdd(Meta.TcpHealthCheckInterval, val.Interval);
                        options.Meta.TryAdd(Meta.DeregisterCriticalServiceAfter, val.DeregisterCriticalServiceAfter);
                        options.Meta.TryAdd(Meta.MachineName, Environment.MachineName);
                        options.Meta.TryAdd(Meta.OSVersion, Environment.OSVersion);
                        options.Meta.TryAdd(Meta.RuntimeVersion, Environment.Version);
                        options.Meta.TryAdd(Meta.AppVersion, Assembly.GetEntryAssembly()?.GetName().Version?.ToString() ?? "<NONE>");
                    });

                services.AddOptions<ConsulHealthCheckOptions>()
                    .Bind(host.Configuration.GetSection(ConsulHealthCheckOptions.Key))
                    .ValidateDataAnnotations()
                    .PostConfigure<IHostEnvironment>((options, env) =>
                    {
                        options.CheckName ??= $"{env.ApplicationName} Health Status";
                        options.Address ??= IPAddress.Loopback;
                    });

                services.AddHostedService<ConsulAppBackgroundService>();

                services.AddSingleton<HealthCheckTcpServer>();

                services.AddTransient<IConsulClient>(p =>
                {
                    var cfg = p.GetRequiredService<IOptions<ConsulClientOptions>>().Value;
                    return new ConsulClient(cfg);
                });

                services.AddSingleton<ConsulAgent>();
            });
    }
}