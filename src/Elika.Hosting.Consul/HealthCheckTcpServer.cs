﻿using System.Net;
using System.Net.Sockets;
using Microsoft.Extensions.Logging;

namespace Elika.Hosting.Consul;

internal class HealthCheckTcpServer : IDisposable
{
    private readonly ILogger<HealthCheckTcpServer> _logger;
    private TcpListener _listener = null!;

    public HealthCheckTcpServer(ILogger<HealthCheckTcpServer> logger)
    {
        _logger = logger;
    }

    public bool IsRunning { get; private set; }

    public void Dispose()
    {
        if (IsRunning)
            Stop();
    }

    private void AcceptSocket(IAsyncResult result)
    {
        _logger.LogTrace("HealthCheck answered");

        if (!IsRunning)
            return;

        var socket = _listener.EndAcceptSocket(result);
        AsyncAcceptSocket();
        socket.Close();
    }

    private void AsyncAcceptSocket()
    {
        _listener.BeginAcceptSocket(AcceptSocket, null);
    }

    public void Run(IPAddress address, int port)
    {
        ArgumentNullException.ThrowIfNull(address);

        if (IsRunning)
            throw new InvalidOperationException("Server already running");

        var ep = new IPEndPoint(address, port);

        _logger.LogInformation("Consul HealthCheck TCP Server starting");
        _logger.LogInformation(
            "HealthCheck TCP Address: {consul_healthcheck_tcp_address}:{consul_healthcheck_tcp_port}", address, port);

        _listener = new TcpListener(ep);
        _listener.Start();
        IsRunning = true;

        _logger.LogInformation("Consul HealthCheck TCP Server started");

        AsyncAcceptSocket();
    }

    public void Stop()
    {
        if (!IsRunning)
            throw new InvalidOperationException("Server not running");

        _logger.LogInformation("Consul HealthCheck TCP Server stopping");

        IsRunning = false;
        _listener.Stop();

        _logger.LogInformation("Consul HealthCheck TCP Server stopped");
    }
}