﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Elika.Hosting.Consul.BackgroundServices;

internal class ConsulAppBackgroundService : BackgroundService
{
    private readonly ConsulAgent _consulAgent;
    private readonly ILogger<ConsulAppBackgroundService> _logger;

    public ConsulAppBackgroundService(ILogger<ConsulAppBackgroundService> logger, ConsulAgent consulAgent)
    {
        _logger = logger;
        _consulAgent = consulAgent;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Consul Service starting");

        await _consulAgent.RunAsync(stoppingToken);

        _logger.LogInformation("Consul Service started");
    }
}