﻿using System.ComponentModel.DataAnnotations;
using System.Net;

namespace Elika.Hosting.Consul.Options;

/// <summary>
///     Health check configurations
/// </summary>
public class ConsulHealthCheckOptions
{
    /// <summary>
    ///     Key in configurations
    /// </summary>
    public const string Key = "Consul:HealthCheck";

    /// <summary>
    ///     Check Name in Consul
    /// </summary>
    [Required]
    public string CheckName { get; set; } = null!;

    /// <summary>
    ///     Check Notes in Consul
    /// </summary>
    [Required]
    public string Notes { get; set; } = "Check service health status via TCP";

    /// <summary>
    ///     Check IP in Consul
    /// </summary>
    [Required]
    public IPAddress Address { get; set; } = null!;

    /// <summary>
    ///     Check TCP Port in Consul
    /// </summary>
    [Required]
    [Range(1, ushort.MaxValue)]
    public int Port { get; set; } = 15_000;

    /// <summary>
    ///     Health check interval
    /// </summary>
    public TimeSpan Interval { get; set; } = TimeSpan.FromSeconds(10);

    /// <summary>
    ///     Service timeout before deregister
    /// </summary>
    public TimeSpan DeregisterCriticalServiceAfter { get; set; } = TimeSpan.FromMinutes(1);
}