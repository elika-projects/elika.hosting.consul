﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Net;
using Consul;

namespace Elika.Hosting.Consul.Options;

/// <summary>
///     Consul integration configuration
/// </summary>
public class ConsulAgentOptions
{
    /// <summary>
    ///     Key in configurations
    /// </summary>
    public const string Key = "Consul";

    /// <summary>
    ///     App Id in Consul
    /// </summary>
    [Required]
    public string AppId { get; set; } = null!;

    /// <summary>
    ///     App Name in Consul
    /// </summary>
    [Required]
    public string AppName { get; set; } = null!;

    /// <summary>
    ///     Meta in Consul
    /// </summary>
    [Required]
    public IDictionary<string, object> Meta { get; set; } = new Dictionary<string, object>();

    /// <summary>
    ///     Address in Consul
    /// </summary>
    [Required]
    public IPAddress Address { get; set; } = IPAddress.Loopback;

    /// <summary>
    ///     Port in Consul
    /// </summary>
    [Range(1, ushort.MaxValue)]
    public int Port { get; set; }

    /// <summary>
    ///     Tags in Consul
    /// </summary>
    [Required] 
    public ICollection<string> Tags { get; set; } = new List<string>();

    /// <summary>
    ///     In docker container flag
    /// </summary>
    public bool IsInDockerContainer { get; set; }
}