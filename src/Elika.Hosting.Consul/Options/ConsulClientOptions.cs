﻿using System.ComponentModel.DataAnnotations;
using Consul;

namespace Elika.Hosting.Consul.Options;

/// <summary>
///     Configurations for connection to Consul
/// </summary>
public class ConsulClientOptions : ConsulClientConfiguration, IValidatableObject
{
    /// <summary>
    ///     Key in configurations
    /// </summary>
    public const string Key = "Consul:Connection";

    /// <summary>
    ///     Root in key/value storage
    /// </summary>
    [Required]
    public string KVRoot { get; set; } = "apps";

    /// <summary>
    ///     App Name in key/value storage
    /// </summary>
    [Required]
    public string KVApp { get; set; } = string.Empty;

    /// <inheritdoc />
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if (Address is null)
            yield return new ValidationResult("Address is null");
    }
}