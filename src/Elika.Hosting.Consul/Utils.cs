﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elika.Hosting.Consul.Options;
using Microsoft.Extensions.Hosting;

namespace Elika.Hosting.Consul
{
    internal class Utils
    {
        public static string GetRoot(ConsulClientOptions options, IHostEnvironment environment)
        {
            var root = string.Join("/", options.KVRoot, environment.ApplicationName) + "/";

            while (root.Contains("//"))
                root = root.Replace("//", "/");

            return root;
        }
    }
}
