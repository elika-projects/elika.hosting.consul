﻿using System.Text;
using Consul;
using Microsoft.Extensions.Configuration;

namespace Elika.Hosting.Consul;

internal class ConsulConfigurationProvider : ConfigurationProvider
{
    private readonly ConsulClient _client;
    private readonly string _root;

    public ConsulConfigurationProvider(ConsulClient client, string root)
    {
        _client = client;
        _root = root;
    }

    public override void Load()
    {
        Data ??= new Dictionary<string, string>();
        Data.Clear();
        var result = _client.KV.List(_root).Result.Response;

        if (result == null)
            return;

        var pairs = result.Where(c => !c.Key.EndsWith("/")).ToArray();

        foreach (var pair in pairs)
        {
            var key = FromConsulKey(pair.Key);
            var value = pair.Value is null ? string.Empty : Encoding.UTF8.GetString(pair.Value);
            Data.Add(key, value);
        }
    }

    private string FromConsulKey(string key)
    {
        return key
            .Replace(_root, string.Empty)
            .Replace("/", ":");
    }
}