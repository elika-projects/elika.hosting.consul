using Elika.Hosting.Consul;
using Elika.Hosting.Consul.Options;
using ExampleApp;
using Microsoft.Extensions.DependencyInjection;

var host = Host.CreateDefaultBuilder(args)
    .UseConsul()
    .ConfigureServices((host, services) =>
    {
        services.AddHostedService<Worker>();

        services.Configure<ConsulAgentOptions>(options =>
        {
            options.Meta.Add("example_key_1", "example_value");
        });

        // Example change configurations
        services.AddOptions<ConsulAgentOptions>()
            .Configure<IHostEnvironment>((options, env) =>
            {
                options.Meta.Add("example_key_2", "example_value");
            });
    })
    .Build();

await host.RunAsync();