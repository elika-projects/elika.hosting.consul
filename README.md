# Elika.Hosting.Consul

## Description

Adds `Consul` support, has the following features:
- Service registration and deregistration
- Get configuration from `KV`
- HealthCheck via TCP
- Integration `IConsulClient` into `DI`

## How to use

Calling `UseConsul` for `IHostBuilder`, example:
```csharp
var host = Host.CreateDefaultBuilder(args)
    .UseConsul()
    ...
    .Build();
```
> The `UseConsul` call must be the first to correct the work


## How to configure

Configurations are presented in 3 options:
- Consul connection configurations [`ConsulClientOptions`](https://gitlab.com/elika-projects/elika.hosting.consul/-/blob/master/src/Elika.Hosting.Consul/Options/ConsulClientOptions.cs)
- App сonfigurations in Consul [`ConsulAgentOptions`](https://gitlab.com/elika-projects/elika.hosting.consul/-/blob/master/src/Elika.Hosting.Consul/Options/ConsulAgentOptions.cs)
- App health check configurations in Consul [`ConsulHealthCheckOptions`](https://gitlab.com/elika-projects/elika.hosting.consul/-/blob/master/src/Elika.Hosting.Consul/Options/ConsulHealthCheckOptions.cs)

For configurations options using `Configure` or `AddOptions<TOptions>().Configure`, example:
```csharp
var host = Host.CreateDefaultBuilder(args)
    .UseConsul()
    .ConfigureServices((host, services) =>
    {
        services.AddOptions<ConsulAgentOptions>()
            .Configure<IHostEnvironment>((options, env) =>
            {
                options.AppName = $"{env.ApplicationName}_example";
                options.Meta.Add("example_key_2", "example_value");
            });
    })
    .Build();
```
or
```csharp
var host = Host.CreateDefaultBuilder(args)
    .UseConsul()
    .ConfigureServices((host, services) =>
    {
        services.Configure<ConsulAgentOptions>(options =>
        {
            options.Meta.Add("example_key_1", "example_value");
        });
    })
    .Build();
```
